from argparse import ArgumentParser
import os, ffmpy

# Constants
IN_EXT  = '.avi'
OUT_EXT = '.mp4'

# Get command line args
parser = ArgumentParser()
parser.add_argument(
	"input_dir",
	help = "specify the input directory for the videos",
	type = str,
)
parser.add_argument(
	"output_dir",
	help = "specify the output directory for the converted videos",
	type = str,
)

args = parser.parse_args()

# Check if the input directory does not exist
if not os.path.isdir(args.input_dir):
	print('The provided input directory was invalid.')
	exit(1)

# Create the output directory if it does not exist
os.makedirs(os.path.dirname(args.output_dir), exist_ok=True)

# Iterate over all the files in the input directory
for file in os.listdir(args.input_dir):
	fname, fext = os.path.splitext(file)

	# Skip if the file does not match the desired extension
	if not fext.lower() == IN_EXT:
		continue

	path_in  = os.path.join(args.input_dir, file)
	path_out = os.path.join(args.output_dir, fname + OUT_EXT)

	# Check if the file already exists in the output directory
	if os.path.isfile(path_out):
		continue

	# Perform conversion
	ffmpy.FFmpeg(
		inputs  = {path_in: None},
		outputs = {path_out: None},
	).run()

print('Finished batch job.')