from argparse import ArgumentParser
import os, ffmpy, threading, queue

class Item:
	def __init__(self, name, path_in, path_out):
		self.name     = name
		self.path_in  = path_in
		self.path_out = path_out

def worker_thread(work_queue, fin_queue, max_count):
	thread = threading.current_thread()
	devnull = open(os.devnull, 'wb')

	while fin_queue.qsize() < max_count:
		item = work_queue.get()

		if fin_queue.qsize() >= max_count:
			break

		print('%s starting conversion for %s' % (thread.name, item.name))

		ffmpy.FFmpeg(
			inputs  = {item.path_in: None},
			outputs = {item.path_out: None},
		).run(stdout=devnull, stderr=devnull)

		print('%s finished converting %s' % (thread.name, item.name))
		fin_queue.put(True)

	devnull.close()

# Constants
IN_EXT  = '.avi'
OUT_EXT = '.mp4'

# Get command line args
parser = ArgumentParser()
parser.add_argument(
	"input_dir",
	help = "specify the input directory for the videos",
	type = str,
)
parser.add_argument(
	"output_dir",
	help = "specify the output directory for the converted videos",
	type = str,
)
parser.add_argument(
	"thread_count",
	help = "the number of worker threads to spawn",
	type = int,
)

args = parser.parse_args()

# Check if the input directory does not exist
if not os.path.isdir(args.input_dir):
	print('The provided input directory was invalid.')
	exit(1)

# Create the output directory if it does not exist
os.makedirs(os.path.dirname(args.output_dir), exist_ok=True)

threads		= []
work_queue 	= queue.Queue()
fin_queue   = queue.Queue()

# Iterate over all the files in the input directory
for file in os.listdir(args.input_dir):
	fname, fext = os.path.splitext(file)

	# Skip if the file does not match the desired extension
	if not fext.lower() == IN_EXT:
		continue

	path_in  = os.path.join(args.input_dir, file)
	path_out = os.path.join(args.output_dir, fname + OUT_EXT)

	# Check if the file already exists in the output directory
	if os.path.isfile(path_out):
		continue

	# Add the work to the queue
	work_queue.put(Item(fname, path_in, path_out))

num_movies = work_queue.qsize()

print('Found', num_movies, 'movies to convert.')

# Create N worker threads
for _ in range(args.thread_count):
	thread = threading.Thread(
		target	= worker_thread,
		args 	= (work_queue, fin_queue, num_movies),
	)

	threads.append(thread)
	thread.start()

# Wait for all work to be done
while not work_queue.empty():
	pass

# Add some dummy work so that the threads will terminate
for _ in range(args.thread_count):
	work_queue.put(True)

for thread in threads:
	thread.join()

print('Finished batch job.')
