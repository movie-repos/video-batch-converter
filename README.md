# Video Batch Converter

This script converts all `.avi` files in the input directory to `.mp4` and places them
in the output directory.

# Running

Clone the repository and enter the root directory:

```sh
$ git clone git@gitlab.com:nightsprol/video-batch-converter.git
$ cd video-batch-converter
```

You will need to have `ffmpy` on your system before you can run the script:

```sh
$ pip install ffmpy
```

To run the script, perform the following:

```sh
$ python run.py [args]
```

You can provide the arg `-h` if you want to see what arguments are available.